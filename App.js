/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import routes from './src/routes/routes.js';
import SInfo from "react-native-sensitive-info";
export default class App extends Component {


  constructor(props) {
    super(props);
    console.disableYellowBox = true;
    this.state = {
      signedIn: false,
      checkedSignIn: false
    };
  }
  componentWillMount() {
    SInfo.getItem("customer_id", {}).then(value => {
      if (value != "" && value != "null" && value != null) {
        this.setState({ signedIn: true, checkedSignIn: true })
      } else {
        this.setState({ signedIn: false, checkedSignIn: true })
      }
    });
  }
  componentDidMount() {
      SplashScreen.hide();
  }
  render() {
    const { checkedSignIn, signedIn } = this.state;

    // If we haven't checked AsyncStorage yet, don't render anything (better ways to do this)
    if (!checkedSignIn) {
      return null;
    }

    const Layout = routes(signedIn);
    return <Layout />;
  }
}


