export class Constants {
    // static LOGIN = "http://www.melonberries.com/agentx/api/rest/v2/customer";
    // static CATEGORY = "https://A821I348LK-dsn.algolia.net/1/indexes/agentx_en_categories_asc/query";
    // static BRAND = "https://A821I348LK-dsn.algolia.net/1/indexes/agentx_en_products/facets/brand/query";
    // static SUPPLIER = "https://A821I348LK-dsn.algolia.net/1/indexes/agentx_en_products/facets/supplier/query";
    // //static HOME = "http://melonberries.com/agentx/api/rest/v2/homescreen/store/en/limreactit/";
    // static HOME = "http://melonberries.com/agentx/api/rest/v2/homescreen/store/en/limit/";
    // static ALLDATA = "https://A821I348LK-dsn.algolia.net/1/indexes/agentx_en_products/query/";
    // static ASCDATA = "https://A821I348LK-dsn.algolia.net/1/indexes/agentx_en_products_name_asc/query";
    // static ADDCART = "http://melonberries.com/agentx/api/rest/v2/addcart";
    // static GETCART = "http://melonberries.com/agentx/api/rest/v2/cart/";
    // static DELETE = "http://melonberries.com/agentx/api/rest/v2/cart/";
    // static UPDATECART = "http://melonberries.com/agentx/api/rest/v2/addcart/";
    // static WISHLISTADD = "http://melonberries.com/agentx/api/rest/v2/wishlist/";
    // static WISHLISTDELETE = "http://melonberries.com/agentx/api/rest/v2/wishlist/"
    // static WISHLISTGET = "http://melonberries.com/agentx/api/rest/v2/wishlist/"
    // static PROCESSORDER = "http://melonberries.com/agentx/api/rest/v2/processorder";
    // static MYNOTIFICATION = "http://www.melonberries.com/agentx/api/rest/v2/notification/";
    // static MYORDER = "http://melonberries.com/agentx/api/rest/v2/customer/orders/";
    // static SINGLEORDER = "http://melonberries.com/agentx/api/rest/v2/order/";
    // static DELETE_NOTIFY="https://melonberries.com/agentx/api/rest/v2/customer/";


    static LOGIN = "http://www.melonberries.com/agentx/api/rest/v2/customer";
    static CATEGORY = "https://A821I348LK-dsn.algolia.net/1/indexes/agentx_en_categories/query";
    static BRAND = "https://A821I348LK-dsn.algolia.net/1/indexes/agentx_en_products/facets/brand/query";
    static SUPPLIER = "https://A821I348LK-dsn.algolia.net/1/indexes/agentx_en_products/facets/supplier/query";
    static HOME = "http://melonberries.com/agentx/api/rest/v2/homescreen/store/en/limit/";
    static ALLDATA = "https://A821I348LK-dsn.algolia.net/1/indexes/agentx_en_products/query/";
    static ASCDATA = "https://A821I348LK-dsn.algolia.net/1/indexes/agentx_en_products_name_asc/query";
    static DSCDATA = "https://A821I348LK-dsn.algolia.net/1/indexes/agentx_en_products_name_desc/query";
    static ADDCART = "http://melonberries.com/agentx/api/rest/v2/addcart";
    static GETCART = "http://melonberries.com/agentx/api/rest/v2/cart/";
    static DELETE = "http://melonberries.com/agentx/api/rest/v2/cart/";
    static UPDATECART = "http://melonberries.com/agentx/api/rest/v2/addcart/";
    static WISHLISTADD = "http://melonberries.com/agentx/api/rest/v2/wishlist/";
    static WISHLISTDELETE = "http://melonberries.com/agentx/api/rest/v2/wishlist/"
    static WISHLISTGET = "http://melonberries.com/agentx/api/rest/v2/wishlist/"
    static PROCESSORDER = "http://melonberries.com/agentx/api/rest/v2/processorder";
    static CANCELORDER = "http://www.melonberries.com/agentx/api/rest/v2/ordercancel/";
    static MYNOTIFICATION = "http://www.melonberries.com/agentx/api/rest/v2/notification/";
    static MYORDER = "http://melonberries.com/agentx/api/rest/v2/customer/orders/";
    static SINGLEORDER = "http://melonberries.com/agentx/api/rest/v2/order/";
    static DELETE_NOTIFY="https://melonberries.com/agentx/api/rest/v2/customer/";
}