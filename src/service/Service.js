import { Constants } from "./Constants";

let headers = {
    Accept: 'application/json',
    "Content-Type": "application/json",
    "Authorization": "agentx 336fac317cb3f7e1446ac01ebb4e0fcf"
};

let header1 = {
    "Content-Type": "application/json",
    "X-Algolia-API-Key": "252b473d0e8689d19fc1033e3a531aad",
    "X-Algolia-Application-Id": "A821I348LK"
};

let header2 = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
};

export default class Service {
    static instance;

    static getInstance = () => {
        if (this.instance == undefined) {
            this.instance = new Service();
        }
        return this.instance;
    }

    _loginPage = (username, password, fcm_Id, successCallback, reqName) => {
        let body = JSON.stringify({
            "api_key": "3f8b7bd0a274d1e398a346454a0e92b5",
            "username": username,
            "password": password,
            "fcm_id": fcm_Id
        });

        this.APICall(
            Constants.LOGIN,
            'POST',
            "",
            headers,
            body,
            successCallback,
            reqName
        );
    }

    getHomeData = (page, successCallback, reqName) => {
        this.APICall(
            Constants.HOME,
            'GET',
            page,
            header2,
            "",
            successCallback,
            reqName
        );
    }
    // _getCategory = (customer_id, successCallback, reqName) => {
    //     this.APICall(
    //         Constants.HOME,
    //         'GET',
    //         customer_id,
    //         header2,
    //         undefined,
    //         successCallback,
    //         reqName
    //     );
    // }
    _getBrand = (category, successCallback, reqName) => {
        let body = JSON.stringify({
            "maxFacetHits" :  100,
            facetFilters: [category]
        });
        this.APICall(
            Constants.BRAND,
            'POST',
            "",
            header1,
            body,
            successCallback,
            reqName
        );
    }
    _getSupplier = (category, brand, successCallback, reqName, ) => {
        let body = JSON.stringify({
            "maxFacetHits" :  100,
            facetFilters: [category, brand]
        });
        this.APICall(
            Constants.SUPPLIER,
            'POST',
            "",
            header1,
            body,
            successCallback,
            reqName
        );
    }

    getAllData = (page, category, brand, supplier, sorts, atoz,ztoa, successCallback, reqName) => {
       
        let url = "";
        if (atoz) {
           // alert(atoz);
            //alert("Ascending data");
            url = Constants.ASCDATA;
        } 
        else if(ztoa){
            //alert(ztoa);
            //alert("Descending data");
            url = "https://A821I348LK-dsn.algolia.net/1/indexes/agentx_en_products_name_desc/query";
        }else {
            url = Constants.ALLDATA;
        }
        let body = JSON.stringify({
            "hitsPerPage": 20,
            "page": page,
            facetFilters: [category, brand, supplier, sorts],
            "facets": [
                "brand",
                "categories.level0",
                "mode",
                "price.AED.default",
                "supplier"
            ]

        });

        this.APICall(
            url,
            'POST',
            "",
            header1,
            body,
            successCallback,
            reqName
        );
    }

    getSearchData = (search, successCallback, reqName) => {

        let body = JSON.stringify({
            "params": "query=" + search,
            facetFilters: []
        });
        this.APICall(
            Constants.ALLDATA,
            'POST',
            "",
            header1,
            body,
            successCallback,
            reqName
        );
    }


    addCart = (customer_ID, product, successCallback, reqName) => {
        let body = JSON.stringify({
            "api_key": "3f8b7bd0a274d1e398a346454a0e92b5",
            "customer_id": "" + customer_ID,
            "product": product,

        });
        this.APICall(
            Constants.ADDCART,
            'POST',
            "",
            headers,
            body,
            successCallback,
            reqName

        );
    }
    _getCart = (value, CartSuccess, reqName) => {

        this.APICall(
            Constants.GETCART,
            'GET',
            value,
            headers,
            undefined,
            CartSuccess,
            reqName
        );
    }

    updateCart = (quote_id, product, successCallback, reqName) => {
        let body = JSON.stringify({
            'api_key': '3f8b7bd0a274d1e398a346454a0e92b5',
            'store_id': '2',
            'product': product,
        });

        this.APICall(
            Constants.UPDATECART,
            'PUT',
            quote_id,
            headers,
            body,
            successCallback,
            reqName
        );
    }

    DeletedItem = (quote_id, item_id, deletedSuccess, reqName) => {
        this.APICall(
            Constants.DELETE,
            'DELETE',
            quote_id + "/product/" + item_id,
            headers,
            undefined,
            deletedSuccess,
            reqName
        );
    }
    processOrder = (body, successCallback, reqName) => {
        this.APICall(
            Constants.PROCESSORDER,
            'POST',
            "",
            headers,
            body,
            successCallback,
            reqName
        );
    }
    cancelOrder = (id,successCallback,reqName) =>{
        let body = JSON.stringify({
            "api_key": "3f8b7bd0a274d1e398a346454a0e92b5"
        });
        this.APICall(
            Constants.CANCELORDER,
            'PUT',
            id,
            headers,
            body,
            successCallback,
            reqName
        );
    }

    _setWishlist = (data, customer_id, successCallback, reqName) => {
        this.APICall(
            Constants.WISHLISTADD,
            'POST',
            customer_id,
            headers,
            data,
            successCallback,
            reqName
        );
    }

    _getWishList = (customer_id, successCallback, reqName) => {
        this.APICall(
            Constants.WISHLISTGET,
            'GET',
            customer_id,
            header2,
            "",
            successCallback,
            reqName
        );
    }
    _removeWishlist = (product_id, customer_id, successCallback, reqName) => {
        this.APICall(
            Constants.WISHLISTDELETE,
            'DELETE',
            customer_id + "/" + "product" + "/" + product_id,
            header2,
            "",
            successCallback,
            reqName
        );
    }
    _myOrder(customer_id, successCallback, reqName) {

        this.APICall(
            Constants.MYORDER,
            'GET',
            customer_id,
            headers,
            "",
            successCallback,
            reqName
        );
    }
    _singleListOrder = (order, successCallback, reqName) => {
        this.APICall(
            Constants.SINGLEORDER,
            'GET',
            order,
            header2,
            "",
            successCallback,
            reqName
        );
    }

    editerOrder = (product, parent_id, successCallback, reqName) => {

        let body = JSON.stringify({
            'items': product,
        });
        this.APICall(
            Constants.SINGLEORDER,
            'PUT',
            parent_id,
            header2,
            body,
            successCallback,
            reqName
        );
    }

    _mynotification(successCallback, reqName, cutomer_id) {

        this.APICall(
            Constants.MYNOTIFICATION,
            'GET',
            cutomer_id,
            header2,
            "",
            successCallback,
            reqName);

    }
    logoutScreen = (successCallback, cutomer_id, fcm, reqName) => {

        this.APICall(
            Constants.DELETE_NOTIFY,
            'DELETE',
            cutomer_id + "/fcm/" + fcm,
            header2,
            "",
            successCallback,
            reqName);
    }

    // _getItems(id, baseClass, successCallback, errorCallback) {
    //     this.callApiOrder(
    //         Constants.SINGLEORDER,
    //         'GET',
    //         id,
    //         successCallback);
    // }


    APICall = (
        url,
        methodType,
        api,
        header,
        data,
        successCallback,
        reqName) => {
        fetch(url + api, {
            method: methodType,
            headers: header,
            body: data
        }).then(response => {
            return response.json();
        }).then(responseData => {
            //alert(JSON.stringify(responseData));
            successCallback(responseData, reqName);
            //alert(JSON.stringify(responseData));
            return responseData;
        }).catch(error => {

        });
    }
    callApiOrder(url, me, id, successCallback) {

        fetch(url + id, {
            method: me,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(response => {
            return response.json();
        }).then(responseData => {
            //alert(JSON.stringify(responseData));
            // base._hideProgress();
            successCallback(responseData);
            return responseData;
        }).catch(error => {
            errorCallback(error);
        });

    }

}