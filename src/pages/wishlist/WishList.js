import React, { Component } from 'react';
//<<<<<<< HEAD
//import { View, Text, ListView, TextInput, Image, NetInfo, RefreshControl, TouchableOpacity } from 'react-native'

import { View, Text, ListView, TextInput, Image, NetInfo, Alert, TouchableOpacity, RefreshControl } from 'react-native'
//>>>>>>> 3cfcc31c4c21ea053a999bd59ee589edf06530ec
import Style from './Styles'
import Base from '../home/Base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icons from 'react-native-vector-icons/MaterialIcons';
import { RadioGroup, RadioButton } from "react-native-flexi-radio-button";
import LinearGradient from "react-native-linear-gradient";
import EntypoIcon from 'react-native-vector-icons/Entypo';
import Service from '../../service/Service';
import SInfo from "react-native-sensitive-info";
import { ResWidth, ResHeight, ResFontSizes } from '../../ui';
import SnackBar from 'react-native-snackbar-component';
export default class WishList extends Base {

    constructor(props) {
        super(props);
        let flagNav;
        this.quote_id = "";
        this.state = {
            wish: "WISHLIST",
            FlatListItems: [],
            loading: false,
            isConnected: true
        }
    }

    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: "100%",
                    backgroundColor: "#607D8B",
                }}
            />
        );
    }

    handleConnectivityChange = isConnected => {
        if (isConnected) {
            this.setState({ isConnected });
        } else {
            this.setState({ isConnected });
        }
    };

    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
    }
    componentDidMount() {
        NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
        SInfo.getItem("customer_id", {}).then(value => {
            if (value != "" && value != "null" && value != null) {
                Service.getInstance()._getWishList(value, this.OnRequestCompletedListener, "WISHLISTS");
            }
        });
        SInfo.getItem("quote_id", {}).then(quote_id => {
            if (quote_id != null) {
                Service.getInstance()._getCart(quote_id, this.OnRequestCompletedListener, "CART");
            }
        });
    }

    OnRequestCompletedListener = (responseData, reqName) => {

        if (reqName == "WISHLISTS") {
            this.setState({ loading: false });
            if (responseData != undefined) {
                var rows = buildSelectedRows(responseData, this.state.selectedItems);
                data = rows.sort(function (a, b) {
                    var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase()
                    if (nameA < nameB) //sort string ascending
                        return -1
                    if (nameA > nameB)
                        return 1
                });
                this.setState({ FlatListItems: data })
            }
        } else if (reqName == "CART") {
            this.setState({ loading: false });
            if (responseData != undefined) {
                var count = 0;
                if (responseData.items != undefined) {
                    for (var i = 0; i < responseData.items.length; i++) {
                        count = count + 1;
                    }
                } else {
                    count = 0;
                }
                this.quote_id = responseData.quote_id
                this.setState({
                    count: count
                });
                this.props.navigation.setParams({ Wishcount: count });
            }
        } else if (reqName == "UPDATE") {
            this.setState({ loading: false });
            const { navigate } = this.props.navigation;
            SInfo.setItem("cart_order", JSON.stringify(responseData), {});
            SInfo.setItem("quote_id", responseData.quote_id, {});
            if (!flagNav) {
                this.componentDidMount();
            } else {
                this.props.navigation.push('MyCart');

            }
        } else if (reqName == "WISHREMOVE") {
            this.componentDidMount();
        }
    }





    // onRowPress = (item, rowID) => {
    //     const Alldata = [...this.state.FlatListItems];
    //     Alldata[rowID] = Object.assign({}, Alldata[rowID], {
    //         selected: !Alldata[rowID].selected,
    //         qty: 1,
    //         totalItem: 1,
    //         itemType: 1
    //     });
    //     this.setState({ FlatListItems: Alldata });
    // }
    // selectType = (value, rowID) => {
    //     const data = [...this.state.FlatListItems];
    //     data[rowID] = Object.assign({}, data[rowID], {
    //         itemType: parseInt(value),
    //         totalItem: data[rowID].qty * parseInt(value)
    //     });
    //     this.setState({ FlatListItems: data });
    // }

    // _handePressAdd = (item, rowID) => {
    //     const data = [...this.state.FlatListItems];
    //     if (data[rowID].selected) {
    //         if (data[rowID].qty > 0) {
    //             data[rowID] = Object.assign({}, data[rowID], {
    //                 qty: data[rowID].qty + 1,
    //                 totalItem: (data[rowID].qty + 1) * data[rowID].itemType
    //             });
    //             this.setState({ FlatListItems: data });
    //         }
    //     } else {
    //         this._showAlert("Please Select Checkbox", "OK");
    //     }
    // }

    // _handePressMinus = (item, rowID) => {
    //     const data = [...this.state.FlatListItems];
    //     if (data[rowID].selected) {
    //         if (data[rowID].qty > 1) {
    //             data[rowID] = Object.assign({}, data[rowID], {
    //                 qty: data[rowID].qty - 1,
    //                 totalItem: (data[rowID].qty - 1) * data[rowID].itemType
    //             });
    //             this.setState({ FlatListItems: data });
    //         }
    //     } else {
    //         this._showAlert("Please Select Checkbox", "OK");
    //     }
    // }

    _handePressMinus = (item, rowID) => {
        const data = [...this.state.FlatListItems];
        
            if (data[rowID].totalItem >= 1) {
                data[rowID] = Object.assign({}, data[rowID], {
                    qty: data[rowID].qty - 1,
                    totalItem: (data[rowID].totalItem - 1),
                    text_value: (data[rowID].totalItem - 1),
                    radioIndex: null,
                    selected:true,
                });
                this.setState({ FlatListItems: data });
            }
        
    }

    _handePressAdd = (item, rowID) => {

        const data = [...this.state.FlatListItems];
       
            if (data[rowID].totalItem >= 0) {
                data[rowID] = Object.assign({}, data[rowID], {
                    qty: data[rowID].qty + 1,
                    totalItem: (data[rowID].totalItem + 1),
                    text_value: (data[rowID].totalItem + 1),
                    radioIndex: null,
                    selected:true,
                });
                this.setState({ FlatListItems: data });
            }
       
    }

    onSubmitBtn = (flag) => {
        flagNav = flag;
        let product = {};
        const select = this.state.FlatListItems.filter(row => row.selected);
        if (select.length > 0) {
            this.setState({ loading: true });
            if (this.quote_id != undefined) {
                select.map((item) => {
                    if (item.totalItem != 0 && item.totalItem != "") {
                        let req = {};
                        req["action"] = "add";
                        req["qty"] = item.totalItem;
                        product[item.product_id] = req;
                    }
                    else {
                        let req = {};
                        req["selected"] = "false";
                        product[item.product_id] = req;
                    }
                });
                Service.getInstance().updateCart(this.quote_id, product, this.OnRequestCompletedListener, "UPDATE");
            } else {
                select.map((item) => {
                    product[item.product_id] = item.totalItem;
                });
                SInfo.getItem("customer_id", {}).then(value => {
                    if (value != "" && value != "null" && value != null) {
                        Service.getInstance().addCart(value, product, this.OnRequestCompletedListener, "UPDATE");
                    }
                });
            }
        } else {
            this._showAlert("Please select minimum one item", "OK")
        }
    }

    onRowPressWishList = (item, rowID) => {
        const data = [...this.state.FlatListItems];
        data[rowID] = Object.assign({}, data[rowID], {
            wishselect: !data[rowID].wishselect,
        });
        this.setState({ FlatListItems: data });
        if (data[rowID].wishselect) {
            //Service.getInstance()._setWishlist(reqVal, this.state.customer_ID, this, this.wishlistSuccess, this._onFailure)
        } else {
            SInfo.getItem("customer_id", {}).then(value => {
                if (value != "" && value != "null" && value != null) {
                    Service.getInstance()._removeWishlist(item.product_id, value, this.OnRequestCompletedListener, "WISHREMOVE")
                }
            });
        }
    }

    edit_text = (value, rowID) => {
        const data = [...this.state.FlatListItems];

        if (value.length > 0) {

            data[rowID] = Object.assign({}, data[rowID], {
                selected: true,
                totalItem: parseInt(value),
                itemType: 1,
                text_value: parseInt(value)
            });
            this.setState({ FlatListItems: data });
        } else {

            data[rowID] = Object.assign({}, data[rowID], {
                itemType: 0,
                totalItem: "",
                selected: true,
                text_value: ""
            });
            this.setState({ FlatListItems: data });
        }


    }

    selectType = (value, rowID, index) => {
        const data = [...this.state.FlatListItems];
        if (value.length > 0) {
            data[rowID] = Object.assign({}, data[rowID], {
                selected: true,
                totalItem: parseInt(value),
                itemType: parseInt(value),
                radioIndex: index,
                radio_value: value,
                text_value: ""
            });
            this.setState({ FlatListItems: data });
        } else {
            data[rowID] = Object.assign({}, data[rowID], {
                itemType: 0,
                totalItem: "",
                selected: true,
            });
            this.setState({ FlatListItems: data });
        }
    }
    onFocus = (text, rowID) => {

        const data = [...this.state.FlatListItems];
        data[rowID] = Object.assign({}, data[rowID],{
            itemType: "",
            totalItem: "",
            radioIndex: null,
            selected: text.length > 0 ? true : false
        });
        this.setState({ FlatListItems: data });

    }

    FlatListItem = (item, rowID) => {

        return (
            <View style={Style.flatitem}>
                <View style={Style.item1}>
                    {/* <View style={Style.checkbox}>
                        <TouchableOpacity onPress={() => this.onRowPress(item, rowID)}>
                            <Icons
                                name={item.selected ? 'check-box' : 'check-box-outline-blank'}
                                color={item.selected ? "#F3476F" : "#a5a5a5"}
                                size={30} />
                        </TouchableOpacity>
                    </View> */}
                    <Text style={Style.checktext}>{item.name}</Text>
                    <View style={Style.checkbox1}>
                        <TouchableOpacity onPress={() => this.onRowPressWishList(item, rowID)}>
                            <Icon
                                name={item.wishselect ? 'heart' : 'heart-outline'}
                                color={item.wishselect ? "#F3476F" : "#F3476F"}
                                size={30} />
                        </TouchableOpacity>

                    </View>
                </View>
                <View style={Style.item2}>
                    <Text style={Style.subtitle}></Text>
                    <View style={Style.item22} >
                        <View style={Style.add}>
                            <TouchableOpacity onPress={() => this._handePressMinus(item, rowID)}>
                                <EntypoIcon
                                    size={28}
                                    color={"#6bbb6e"}
                                    name="minus" />
                            </TouchableOpacity>
                        </View>
                        <View style={Style.add1}>
                            <TextInput
                                style={Style.addtext}
                                maxLength={6}
                                onChangeText={(text) => this.edit_text(text, rowID)}
                                keyboardType='numeric'
                                value={(item.selected ? item.totalItem : item.totalItem).toString()}
                                onFocus={(text) => this.onFocus(text, rowID)}
                            />
                        </View>
                        <View style={Style.add}>
                            <TouchableOpacity onPress={() => this._handePressAdd(item, rowID)}>
                                < EntypoIcon
                                    size={28}
                                    color={"#6bbb6e"}
                                    name="plus" />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={Style.item3}>
                    <RadioGroup
                        size={ResWidth(5)}
                        thickness={1.2}
                        color="#5e5e5e"
                        activeColor='#FC5763'
                        style={Style.radiogroup}
                        selectedIndex={item.selected ? item.radioIndex : null}
                        onSelect={(index, value) => this.selectType(value, rowID, index)}>
                        <RadioButton
                            style={{ alignItems: 'center' }}
                            value='1'>
                            <Text style={Style.radiotext}>{"1"}</Text>
                        </RadioButton>
                        <RadioButton
                            style={{ alignItems: 'center' }}
                            value='10'>
                            <Text style={Style.radiotext}>{"10"}</Text>
                        </RadioButton>
                        <RadioButton
                            style={{ alignItems: 'center' }}
                            value='25'>
                            <Text style={Style.radiotext}>{"25"}</Text>
                        </RadioButton>
                        <RadioButton
                            style={{ alignItems: 'center' }}
                            value='50'>
                            <Text style={Style.radiotext}>{"50"}</Text>
                        </RadioButton>
                        <RadioButton
                            style={{ alignItems: 'center' }}
                            value='100'>
                            <Text style={Style.radiotext}>{"100"}</Text>
                        </RadioButton>
                    </RadioGroup>
                </View>
            </View>
        );
    }

    onSelect = (index, value) => {
        this.setState({
            text: `Selected index: ${index} , value: ${value}`
        })
    }
    onRefresh() {
        SInfo.getItem("customer_id", {}).then(value => {
            if (value != "" && value != "null" && value != null) {
                Service.getInstance()._getWishList(value, this.OnRequestCompletedListener, "WISHLISTS");
            }
        });
        SInfo.getItem("quote_id", {}).then(quote_id => {
            if (quote_id != null) {
                Service.getInstance()._getCart(quote_id, this.OnRequestCompletedListener, "CART");
            }
        });
    }
    render() {
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        return (
            <View style={{ flex: 1, flexDirection: "column", backgroundColor: 'white' }}>
                {this.state.FlatListItems.length > 0 ?
                    < View style={Style.root}>
                        <View style={Style.views}>
                            <ListView
                                dataSource={ds.cloneWithRows(this.state.FlatListItems)}
                                onEndReached={this.onEndReached}
                                onEndReachedThreshold={0.5}
                                refreshControl={
                                    <RefreshControl
                                        refreshing={this.state.refreshing}
                                        onRefresh={this.onRefresh.bind(this)}
                                    />
                                }
                                // extraData={this.state.itemUpdated}
                                //  onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false }}
                                //renderFooter={() => this.state.loading ? null : <ActivityIndicator size="small" animating />}
                                renderSeparator={this.FlatListItemSeparator}
                                renderRow={(rowData, sectionID, rowID, highlightRow) => this.FlatListItem(rowData, rowID)} />
                        </View>
                        <View style={Style.view1}>
                            <View style={Style.v1}>
                                <TouchableOpacity
                                    onPress={() => this.onSubmitBtn(false)}>
                                    <LinearGradient
                                        colors={['#FA486F', '#Fc5766', '#FD6A61']}
                                        start={{ x: 0.0, y: 0.25 }}
                                        end={{ x: 1.0, y: 1.0 }}
                                        style={Style.button}
                                        locations={[0, 0.5, 0.6]}>
                                        <Text
                                            style={Style.buttontext}>ADD TO CART</Text>
                                    </ LinearGradient>
                                </TouchableOpacity>
                            </View>
                            <View style={Style.v1}>
                                <TouchableOpacity onPress={() => this.onSubmitBtn(true)}>
                                    <LinearGradient
                                        colors={['#FA486F', '#Fc5766', '#FD6A61']}
                                        start={{ x: 0.0, y: 0.25 }}
                                        end={{ x: 1.0, y: 1.0 }}
                                        style={Style.button}
                                        locations={[0, 0.5, 0.6]}>
                                        <Text style={Style.buttontext}>CHECKOUT</Text>
                                    </ LinearGradient>
                                </TouchableOpacity>
                            </View>

                        </View>
                        {this.state.loading ? <Base /> : null}
                    </View > :
                    <View style={Style.nowishlist}>
                        <Image
                            source={{ uri: 'http://melonberries.com/agentx/appimages/nowishlist.png' }}
                            style={{ width: ResWidth(50), height: ResHeight(50), borderRadius: 50 / 2, backgroundColor: '#ffffff' }}
                            resizeMode='cover' />
                        <Text style={{ fontSize: 15, fontWeight: '800' }}>No items yet</Text>
                        <Text>simply browse and tap on the heart icon</Text>
                    </View>
                }
                <SnackBar
                    visible={!this.state.isConnected}
                    backgroundColor={"red"}
                    textMessage="No Internet Connection" />
            </View>
        );
    }

}

function buildSelectedRows(items, selectedItems) {
    const rows = items.map(item =>
        Object.assign({}, item, {
            selected: false,//
            itemType: 1,
            qty: 0,
            totalItem: 0,
            wishselect: true,// selectedItems.some(i => i.product_id == item.objectID),
        }),
    );
    return rows;
}