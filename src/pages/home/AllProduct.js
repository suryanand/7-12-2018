import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    ListView,
    ActivityIndicator,
    KeyboardAvoidingView,
    TouchableOpacity,
    NetInfo,
    TextInput
} from 'react-native'
import Style from './ProductStyle';
import Base from './Base';
import CatogorysDialog from "../../ui/CategorysDialog";
import BrandDialog from "../../ui/BrandDialog";
import SupplierDialog from "../../ui/SupplierDialog";
import SortDialog from "../../ui/SortDialog";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icons from 'react-native-vector-icons/MaterialIcons';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button'
import { ResWidth, ResHeight, ResFontSizes } from '../../ui';
import LinearGradient from "react-native-linear-gradient";
import Service from "../../service/Service";
import SInfo from "react-native-sensitive-info";
import { SearchBar } from 'react-native-elements';
import SnackBar from 'react-native-snackbar-component'
import SideMenu from "../sideMenu/SideMenu";
import sortJsonArray from 'sort-json-array';
export default class AllProduct extends Base {
    static ins;
    constructor() {
        super();
        this.onEndReachedCalledDuringMomentum = true;
        let flagNav;
        let quote_id;

        this.categorys = [];
        this.brands = [];
        this.suppliers = [];
        this.sorts = [];
        this.atoz = false;
        this.state = {
            page: 0,
            searchFlag: false,
            modalcat: false,
            Category: false,
            modalbrand: false,
            brand: false,
            modelsupplier: false,
            supplier: false,
            modelsort: false,
            sort: false,
            categorySelectedItems: [],
            brandSelectedItems: [],
            supplierSelectedItems: [],
            sortSelectedItems: [],
            Alldata: [],
            categotyData: [],
            BrandData: [],
            SupplierData: [],
            SortData: [
                {
                    "value": "BestSeller",
                },
                {
                    "value": "A to Z",
                },
                {
                    "value": "Z to A",
                },
            ],
            loading: false,
            selectedItems: [],
            count: 0,
            isConnected: true,
            isRefreshing: false,
            quantity: 0,
            qty_txt: "0"
        };
        ins = this;

    }
    componentDidMount() {
        SideMenu.getName();
        NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
    }
    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
    }

    handleConnectivityChange = isConnected => {
        if (isConnected) {
            this.setState({ isConnected });
        } else {
            this.setState({ isConnected });
        }
    };


    callBack = () => {
        SInfo.getItem("customer_id", {}).then(customer_id => {
            if (customer_id != "" && customer_id != "null" && customer_id != null) {
                Service.getInstance().getHomeData("1", this.OnRequestCompletedListener, "Home");
                Service.getInstance()._getWishList(customer_id, this.OnRequestCompletedListener, "GETWISHLIST");
                SInfo.getItem("quote_id", {}).then(quote_id => {
                    if (quote_id != null) {
                        Service.getInstance()._getCart(quote_id, this.onCartData, "CART");
                    }
                });
            }
        });

    }

    onCartData = (responseData, reqName) => {
        if (reqName == "CART") {
            this.setState({ loading: false });
            if (responseData != undefined) {
                var count = 0;
                if (responseData.items != undefined) {
                    for (var i = 0; i < newFunction(responseData); i++) {
                        count = count + 1;
                    }
                } else {
                    count = 0;
                }
                this.quote_id = responseData.quote_id;
                this.setState({
                    count: count
                });
                this.props.navigation.setParams({ count: count });
            }
        }
    }



    OnRequestCompletedListener = (responseData, reqName) => {

        if (reqName == "Home") {
            data = sortJsonArray(responseData.categories, 'name', 'asc');
            this.setState({
                categotyData: data,
            });
        } else if (reqName == "GETWISHLIST") {
            if (responseData != undefined) {
                this.setState({ selectedItems: responseData });
                const Selectdata = this.props.navigation.getParam('data', []);
                const index = this.props.navigation.getParam('index', []);
                if (index == 1) {
                    this.setState({ sortSelectedItems: Selectdata });
                    this.sorts.push("mode:BestSeller");
                } else if (index == 2) {

                } else if (index == 3) {
                    this.setState({ categorySelectedItems: Selectdata });
                    this.categorys.push("categories.level0:" + Selectdata[0].label.name);
                }
                this.getBrands("1");
                this.getSupplier("1");
                this.fetchData();
            }
        }
    }
    componentWillMount() {
        this.setState({ loading: true });
        this.callBack();

        ins.props.navigation.setParams({ flag: false });
    }

    // onSubmitBtn = (flag) => {
    //     flagNav = flag;
    //     let product = {};
    //     const select = this.state.Alldata.filter(row => row.selected);
    //     if (select.length > 0) {
    //         this.setState({ loading: true });
    //         if (this.quote_id != undefined) {
    //             select.map((item) => {
    //                 let req = {};
    //                 req["action"] = "add";
    //                 req["qty"] = item.totalItem;
    //                 product[item.objectID] = req;
    //             });
    //             Service.getInstance().updateCart(this.quote_id, product, this.onCartSubmit, "SUBMIT");
    //         } else {
    //             select.map((item) => {
    //                 product[item.objectID] = item.totalItem;
    //             });
    //             SInfo.getItem("customer_id", {}).then(value => {
    //                 if (value != "" && value != "null" && value != null) {
    //                     Service.getInstance().addCart(value, product, this.onCartSubmit, "SUBMIT");
    //                 }
    //             });
    //         }
    //     } else {
    //         this._showAlert("Please select minimum one item", "OK")
    //     }
    // }

    onSubmitBtn = (flag) => {
        flagNav = flag;
        let product = {};
        const select = this.state.Alldata.filter(row => row.selected);
        //alert(JSON.stringify(select));
        if (select.length > 0) {
            this.setState({ loading: true });
            if (this.quote_id != undefined) {
                select.map((item) => {
                    if (item.totalItem != 0 && item.totalItem != "") {
                        let req = {};
                        req["action"] = "add";
                        req["qty"] = item.totalItem;
                        product[item.objectID] = req;
                    }
                    // else {
                    //     let req = {};
                    //     req["action"] = "add";
                    //     req["qty"] = parseInt(item.radio_value);
                    //     product[item.objectID] = req;
                    // }
                });
                Service.getInstance().updateCart(this.quote_id, product, this.onCartSubmit, "SUBMIT");

            } else {
                select.map((item) => {
                    product[item.objectID] = item.totalItem;
                });
                SInfo.getItem("customer_id", {}).then(value => {
                    if (value != "" && value != "null" && value != null) {
                        Service.getInstance().addCart(value, product, this.onCartSubmit, "SUBMIT");
                    }
                });
            }
        } else {
            this._showAlert("Please select minimum one item", "OK")
        }
    }

    onCartSubmit = (responseData, reqName) => {
        if (reqName == "SUBMIT") {
            this.setState({ loading: false });
            SInfo.setItem("cart_order", JSON.stringify(responseData), {});
            SInfo.setItem("quote_id", responseData.quote_id, {});
            const data = buildSelectedRows(this.state.Alldata, this.state.selectedItems);
            this.setState({ Alldata: data });
            if (!flagNav) {
                this.callBack();
            } else {
                const rows = this.state.Alldata.map(item =>
                    Object.assign({}, item, {
                        selected: false,
                        itemType: 0,
                        qty: 0,
                        totalItem: 0,
                        radioIndex: null
                    }));
                this.setState({ Alldata: rows });
                this.props.navigation.navigate("MyCart", { callBack: this.callBack });
            }
        }

    }

    getBrands = (index) => {
        if (this.state.categorySelectedItems.length > 0) {
            this.categorys = [];
            this.state.categorySelectedItems.map((item) => {
                this.categorys.push("categories.level0:" + item.label.name);
            });
        }
        if (index == "1") {
            Service.getInstance()._getBrand(this.categorys, this.onBrand, "Brand");
        } else if (index == "2") {
            this.fetchData();
            Service.getInstance()._getBrand(this.categorys, this.onBrand, "Brand");
            this.getSupplier("1")
        }

    }

    onBrand = (responseData, reqName) => {
        if (reqName == "Brand") {
            if (responseData != undefined) {
                data = sortJsonArray(responseData.facetHits, 'value', 'asc');
                this.setState({ BrandData: data });

            }
        }
    }

    getSupplier = (index) => {
        if (this.state.brandSelectedItems.length > 0) {
            this.state.brandSelectedItems.map((item) => {
                this.brands.push("brand:" + item.label.value);
            });
        }
        this.getBrands("1")
        if (index == "1") {
            Service.getInstance()._getSupplier(this.categorys, this.brands, this.onSupplier, "Supplier");
        } else {
            this.fetchData();
            Service.getInstance()._getSupplier(this.categorys, this.brands, this.onSupplier, "Supplier");
        }
    }

    onSupplier = (responseData, reqName) => {
        if (reqName == "Supplier") {

            if (responseData != undefined) {
                data = sortJsonArray(responseData.facetHits, 'value', 'asc');
                this.setState({ SupplierData: responseData.facetHits });
            }
        }
    }

    getSort = () => {
        if (this.state.supplierSelectedItems.length > 0) {
            this.state.supplierSelectedItems.map((item) => {
                this.suppliers.push("supplier:" + item.label.value);
            });
        }
        this.fetchData();
    }

    getSorts = () => {
        // if (this.state.sortSelectedItems.length > 0) {
        //     this.state.sortSelectedItems.map((item) => {
        //         if (item.value == "1" ) {
        //             this.atoz = true;
        //             //this.ztoa=false;
        //         }
        //         // else if(item.value == "2"){
        //         //     this.atoz = false;
        //         //     this.ztoa=true;
        //         // } 
        //         else {
        //             this.atoz = false;
        //         }
        //         if (item.value == "0") {
        //             this.sorts.push("mode:" + item.label.value);
        //         }
        //         else{
        //             this.sorts = [];
        //         }
        //     });
        // }
        // this.fetchData();

        if (this.state.sortSelectedItems.length > 0) {

            if (this.state.sortSelectedItems.length == "2") {
                this.state.sortSelectedItems.map((item) => {
                    if (this.state.sortSelectedItems[0].value == "0" && this.state.sortSelectedItems[1].value == "1") {
                        this.atoz = true;
                        this.ztoa = false;
                        this.sorts.push("mode: BestSeller");
                    }

                    else if (this.state.sortSelectedItems[0].value == "0" && this.state.sortSelectedItems[1].value == "2") {
                        this.ztoa = true;
                        this.atoz = false;
                        this.sorts.push("mode: BestSeller");
                    }
                    else {
                        this.atoz = false;
                        this.ztoa = false;
                        this.sorts = [];
                    }
                });
            }
            else {
                this.state.sortSelectedItems.map((item) => {
                    if (this.state.sortSelectedItems[0].value == "0") {
                        this.sorts.push("mode: BestSeller");
                        this.ztoa = false;
                        this.atoz = false;
                    }
                    else if (this.state.sortSelectedItems[0].value == "1") {
                        this.ztoa = false;
                        this.atoz = true;
                    }
                    else if (this.state.sortSelectedItems[0].value == "2") {
                        this.ztoa = true;
                        this.atoz = false;
                    }
                    else {
                        this.atoz = false;
                        this.ztoa = false;
                        this.sorts = [];
                    }
                });
            }

        }


        this.fetchData();
    }

    fetchData = () => {
        Service.getInstance().getAllData(this.state.page, this.categorys, this.brands, this.suppliers, this.sorts, this.atoz, this.ztoa, this.onAllData, "All");
    }

    onAllData = (responseData, reqName) => {
        const { Alldata, page } = this.state;
        this.setState({ loading: false });
        if (responseData != undefined) {
            if (responseData.nbPages >= page) {
                const rows = buildSelectedRows(responseData.hits, this.state.selectedItems);
                this.setState({
                    Alldata: [...Alldata, ...rows],
                    isRefreshing: false,
                });
                this.setState({ rows: rows });
            }
        }
    }

    onWishlist = (responseData, reqName) => {
        if (reqName == "Wishlist") {
            this.setState({ loading: false });
        }
    }


    _handePressAdd = (item, rowID) => {
        const data = [...this.state.Alldata];
        //alert(JSON.stringify(item));
            if (data[rowID].totalItem >= 0) {
                data[rowID] = Object.assign({}, data[rowID], {
                    selected: true,
                    qty: data[rowID].qty + 1,
                    radioIndex: null,
                    totalItem: (parseInt(data[rowID].totalItem + 1)),
                    text_value: (data[rowID].totalItem + 1)
                    
                });
                //alert(data[rowID].totalItem);
                this.setState({ Alldata: data });
            }
        
    }

    _handePressMinus = (item, rowID) => {
        const data = [...this.state.Alldata];
       
            if (data[rowID].totalItem >= 1) {
                data[rowID] = Object.assign({}, data[rowID], {
                    selected: true,
                    qty: data[rowID].qty - 1,
                    radioIndex: null,
                    totalItem: (data[rowID].totalItem - 1),
                    text_value: (data[rowID].totalItem - 1)
                });
                this.setState({ Alldata: data });
            }
        
    }

    // onRowPress = (item, rowID) => {

    //     const Alldata = [...this.state.Alldata];
    //     alert(Alldata[rowID].selected);
    //     Alldata[rowID] = Object.assign({}, Alldata[rowID], {
    //         selected: true,
    //         qty: 1,
    //         totalItem: 1,
    //         itemType: 1
    //     });
    //     this.setState({ Alldata: Alldata });
    // }
    edit_text = (value, rowID) => {
       
        const data = [...this.state.Alldata];

        if (value.length > 0) {
            data[rowID] = Object.assign({}, data[rowID], {
                selected: true,
                totalItem: parseInt(value),
                itemType: 1,
                text_value: parseInt(value),
                radio_value: "",
            });
            this.setState({ Alldata: data });
            //alert(JSON.stringify(this.state.Alldata[rowID]));
        } else {
            data[rowID] = Object.assign({}, data[rowID], {
                itemType: 0,
                totalItem: "",
                selected: true,
                text_value: ""
            });
            this.setState({ Alldata: data });
        }
    }

    onFocus = (text, rowID) => {
        const data = [...this.state.Alldata];
        data[rowID] = Object.assign({}, data[rowID],{
            selected:true,
            totalItem: "",
            itemType: "",
            radioIndex: null,
            radio_value: "",
            //text_value:"0"
        });  
        this.setState({ Alldata: data });
    }

    selectType = (value, rowID, index) => {
        const data = [...this.state.Alldata];
        if (value.length > 0) {
            data[rowID] = Object.assign({}, data[rowID], {
                selected: true,
                totalItem: parseInt(value),
                itemType: parseInt(value),
                radioIndex: index,
                radio_value: value,
                text_value: 0
            });
            this.setState({ Alldata: data });
        } else {
            data[rowID] = Object.assign({}, data[rowID], {
                itemType: 0,
                totalItem: "",
                selected: true,
            });
            this.setState({ Alldata: data });
        }
    }

    onRowPressWishList = (item, rowID) => {
        const data = [...this.state.Alldata];
        data[rowID] = Object.assign({}, data[rowID], {
            wishselect: !data[rowID].wishselect,
        });
        this.setState({ Alldata: data });

        SInfo.getItem("customer_id", {}).then(value => {
            if (value != "" && value != "null" && value != null) {
                if (data[rowID].wishselect) {
                    reqVal = JSON.stringify({
                        "product_id": item.objectID
                    });
                    Service.getInstance()._setWishlist(reqVal, value, this.onWishlist, "Wishlist")
                } else {
                    Service.getInstance()._removeWishlist(item.objectID, value, this.onWishlist, "Wishlist")

                }
            }
        });

    }


    findCategory = (result) => {
        this.categorys = [];
        this.brands = [];
        this.suppliers = [];
        this.setState({ Alldata: [] });
        if (result != undefined) {
            this.Categorybg(false);
            this.setState({ loading: true });
            this.setState({ categorySelectedItems: [] });
            this.setState({ brandSelectedItems: [] });
            this.setState({ supplierSelectedItems: [] });
            this.setState(state => ({
                categorySelectedItems: result.selectedItems
            }), () => this.getBrands("2"));
        }




    }

    findBrand = (result) => {
        this.brands = [];
        this.suppliers = [];
        this.setState({ Alldata: [] });
        if (result != undefined) {
            this.Brandbg(false);
            this.setState({ loading: true });
            this.setState({ supplierSelectedItems: [] });
            this.setState(state => ({
                brandSelectedItems: result.selectedItems
            }), () => this.getSupplier("2"));
        }
    }

    findSuplier = (result) => {
        this.suppliers = [];
        this.setState({ Alldata: [] });
        if (result != undefined) {
            this.Supplierbg(false);
            this.setState({ loading: true });
            this.setState(state => ({
                supplierSelectedItems: result.selectedItems
            }), () => this.getSort());

        }
    }
    findSort = (result) => {
        if (result != undefined) {
            this.setState({ Alldata: [] });
            this.sorts = [];
            this.atoz = false;
            this.ztoa = false;
            this.Sortbg(false);
            this.setState({ loading: true });
            this.setState(state => ({
                sortSelectedItems: result.selectedItems
            }), () => this.getSorts());

        }
    }

    onEndReached = () => {
        if (this.state.Alldata.length > 5) {
            if (!this.onEndReachedCalledDuringMomentum) {
                this.setState(state => ({
                    isRefreshing: true,
                    page: this.state.page + 1
                }), () => this.fetchData());
                this.onEndReachedCalledDuringMomentum = true;
            }
        }
    }

    SearchFilterFunction = (text) => {
        this.setState({ Alldata: [] });
        this.setState({ loading: false });
        Service.getInstance().getSearchData(text, this.onAllData, "SEARCH");
    }

    static searchMethod(flag) {
        if (flag) {
            ins.setState({
                searchFlag: true,
            });
        } else {
            ins.setState({
                searchFlag: false,
                data: instance.arrayholder
            });
        }
        ins.props.navigation.setParams({ flag: flag });
    }
    renderFooter() {
        return this.state.isRefreshing ? <ActivityIndicator size="small" animating /> : null
    }
    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: "100%",
                    backgroundColor: "#607D8B",
                }}
            />
        );
    }
    

    onTextFieldEndEditing = (text, rowID) => {

        const data = [...this.state.Alldata];
        let existingData = data[rowID]

        data[rowID] = Object.assign({}, data[rowID], {
            totalItem: (existingData.totalItem === "") ? "" : existingData.totalItem,
            /*selected already false change in drop */
        });
        this.setState({ Alldata: data });
    }
    //onPress={() => this.onRowPress(item, rowID)}
    FlatListItem = (item, rowID) => {
        return (
            <View style={Style.flatitem}>
                <View style={Style.item1}>

                    {/* <View style={Style.iconview}>
                            <Icons
                                name={item.selected ? 'check-box' : 'check-box-outline-blank'}
                                color={item.selected ? "#F3476F" : "#a5a5a5"}
                                size={30} />
                        </View> */}
                    <Text style={Style.checktext}>{item.name}</Text>
                    {item.mode == "BestSeller" ?
                        <View style={Style.bestseller}>
                            <Text style={Style.besttext}>BestSeller</Text>
                        </View> : null}

                    <View style={Style.checkbox1}>
                        <TouchableOpacity onPress={() => this.onRowPressWishList(item, rowID)}>
                            <Icon
                                name={item.wishselect ? 'heart' : 'heart-outline'}
                                color={item.wishselect ? "#F3476F" : "#F3476F"}
                                size={30} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={Style.item2}>
                    <Text style={Style.subtitle}>{item.supplier}</Text>
                    <View style={Style.item22} >
                        <EntypoIcon
                            style={Style.add}
                            onPress={() => this._handePressMinus(item, rowID)}
                            size={28}
                            color={"#6bbb6e"}
                            name="minus" />
                        <View style={Style.add1}>

                            <TextInput
                                style={Style.addtext}
                                maxLength={6}
                                onChangeText={(text) => this.edit_text(text, rowID)}
                                keyboardType='numeric'
                                value={(item.selected ? item.totalItem : 0).toString()}
                                onFocus={(text) => this.onFocus(text, rowID)}
                                //onEndEditing={(text) => { this.onTextFieldEndEditing(text, rowID) }}
                            />
                            {/* <Text style={Style.addtext}>{item.selected ? item.totalItem : 0}</Text> */}
                        </View>
                        < EntypoIcon
                            style={Style.add}
                            onPress={() => this._handePressAdd(item, rowID)}
                            size={28}
                            color={"#6bbb6e"}
                            name="plus" />

                    </View>
                </View>
                <View style={Style.item3}>
                    <RadioGroup
                        size={ResWidth(5)}
                        thickness={1.2}
                        color="#5e5e5e"
                        selectedIndex={item.selected ? item.radioIndex : null}
                        activeColor='#FC5763'
                        style={Style.radiogroup}
                        onSelect={(index, value) => this.selectType(value, rowID, index)}>
                        <RadioButton
                            style={{ alignItems: 'center' }}
                            value='1'>
                            <Text style={Style.radiotext}>{"1"}</Text>
                        </RadioButton>
                        <RadioButton
                            style={{ alignItems: 'center' }}
                            value='10'>
                            <Text style={Style.radiotext}>{"10"}</Text>
                        </RadioButton>
                        <RadioButton
                            style={{ alignItems: 'center' }}
                            value='25'>
                            <Text style={Style.radiotext}>{"25"}</Text>
                        </RadioButton>
                        <RadioButton
                            style={{ alignItems: 'center' }}
                            value='50'>
                            <Text style={Style.radiotext}>{"50"}</Text>
                        </RadioButton>
                        <RadioButton
                            style={{ alignItems: 'center' }}
                            value='100'>
                            <Text style={Style.radiotext}>{"100"}</Text>
                        </RadioButton>
                    </RadioGroup>
                </View>
            </View>
        );
    }

    render() {
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        return (
            <KeyboardAvoidingView style={{ flex: 1 }}>
                <View style={Style.root}>{
                    this.state.searchFlag ?
                        <SearchBar
                            round
                            clearIcon={true}
                            onChangeText={(text) => this.SearchFilterFunction(text)}
                            searchIcon={{ size: 26, justifyContent: 'center', alignItems: "center" }}
                            containerStyle={Style.searchs}
                            placeholderTextColor={"#747373"}
                            inputStyle={{ backgroundColor: '#D8D5D5', color: "#747373", justifyContent: 'center', alignItems: "center", fontSize: 15 }}
                            placeholder='Search Here...' />
                        :
                        <View style={Style.view1}>
                            <TouchableOpacity
                                style={{ flex: 2.5, }}
                                onPress={() => { this.Categorybg(true); }}>
                                <View style={{
                                    flex: 2.5,
                                    backgroundColor: this.state.Category ? "#F3476F" : "#E8E8E8",
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}>
                                    <Text style={Style.tabtext} >Category</Text>
                                    <Icon name='chevron-down' size={32} />
                                </View>
                            </TouchableOpacity>

                            <CatogorysDialog
                                title={'Category'}
                                colorAccent={"#999797"}
                                items={this.state.categotyData.map((row, index) => ({ value: index, label: row }))}
                                visible={this.state.modalcat}
                                selectedItems={this.state.categorySelectedItems}
                                onCancel={() => this.Categorybg(false)}
                                onOk={(result) => this.findCategory(result)} />
                            <TouchableOpacity
                                style={{ flex: 2.5, }}
                                onPress={() => { this.Brandbg(true); }}>
                                <View style={{
                                    flex: 2.5,
                                    backgroundColor: this.state.brand ? "#F3476F" : "#E8E8E8",
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}>
                                    <Text style={Style.tabtext} >Brand</Text>
                                    <Icon name='chevron-down' size={32} />
                                </View>
                            </TouchableOpacity>
                            <BrandDialog
                                title={'Brand'}
                                colorAccent={"#999797"}
                                items={this.state.BrandData.map((row, index) => ({ value: index, label: row }))}
                                visible={this.state.modalbrand}
                                selectedItems={this.state.brandSelectedItems}
                                onCancel={() => this.Brandbg(false)}
                                onOk={(result) => this.findBrand(result)} />
                            <TouchableOpacity
                                style={{ flex: 2.5, }}
                                onPress={() => { this.Supplierbg(true); }}>
                                <View style={{
                                    flex: 2.5,
                                    backgroundColor: this.state.supplier ? "#F3476F" : "#E8E8E8",
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}>
                                    <Text style={Style.tabtext} >Supplier</Text>
                                    <Icon name='chevron-down' size={32} />
                                </View>
                            </TouchableOpacity>
                            <SupplierDialog
                                title={'Supplier'}
                                colorAccent={"#999797"}
                                items={this.state.SupplierData.map((row, index) => ({ value: index, label: row }))}
                                visible={this.state.modelsupplier}
                                selectedItems={this.state.supplierSelectedItems}
                                onCancel={() => this.Supplierbg(false)}
                                onOk={(result) => this.findSuplier(result)} />
                            <TouchableOpacity
                                style={{ flex: 2.5, }}
                                onPress={() => { this.Sortbg(true); }}>
                                <View style={{
                                    flex: 2.5,
                                    backgroundColor: this.state.sort ? "#F3476F" : "#E8E8E8",
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}>
                                    <Text style={Style.tabtext} >Sort</Text>
                                    <Icon name='chevron-down' size={32} />
                                </View>
                            </TouchableOpacity>
                            <SortDialog
                                title={'Sort'}
                                colorAccent={"#999797"}
                                items={this.state.SortData.map((row, index) => ({ value: index, label: row }))}
                                visible={this.state.modelsort}
                                selectedItems={this.state.sortSelectedItems}
                                onCancel={() => this.Sortbg(false)}
                                onOk={result => this.findSort(result)} />
                        </View>}
                    {this.state.Alldata.length > 0 ?
                        <View style={Style.viewsd}>
                            <View style={Style.view2}>
                                <ListView
                                    dataSource={ds.cloneWithRows(this.state.Alldata)}
                                    onEndReached={() => this.onEndReached()}
                                    onEndReachedThreshold={5}
                                    onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false }}
                                    renderFooter={() => this.renderFooter()}
                                    renderSeparator={this.FlatListItemSeparator}
                                    renderRow={(rowData, sectionID, rowID, highlightRow) => this.FlatListItem(rowData, rowID)} />
                            </View>
                            <View style={Style.view1}>
                                <View style={Style.v1}>
                                    <TouchableOpacity
                                        onPress={() => this.onSubmitBtn(false)}>
                                        <LinearGradient
                                            colors={['#FA486F', '#Fc5766', '#FD6A61']}
                                            start={{ x: 0.0, y: 0.25 }}
                                            end={{ x: 1.0, y: 1.0 }}
                                            style={Style.button}
                                            locations={[0, 0.5, 0.6]}>
                                            <Text
                                                style={Style.buttontext}>ADD TO CART</Text>
                                        </ LinearGradient>
                                    </TouchableOpacity>
                                </View>
                                <View style={Style.v1}>
                                    <TouchableOpacity onPress={() => this.onSubmitBtn(true)}>
                                        <LinearGradient
                                            colors={['#FA486F', '#Fc5766', '#FD6A61']}
                                            start={{ x: 0.0, y: 0.25 }}
                                            end={{ x: 1.0, y: 1.0 }}
                                            style={Style.button}
                                            locations={[0, 0.5, 0.6]}>
                                            <Text style={Style.buttontext}>CHECKOUT</Text>
                                        </ LinearGradient>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View> :
                        <View style={Style.nodatas}>
                            <Image
                                source={{ uri: 'http://melonberries.com/agentx/appimages/nosearch.png' }}
                                style={{ width: ResWidth(50), height: ResHeight(50), borderRadius: 200 / 2, backgroundColor: '#ffffff' }}
                                resizeMode='cover' />
                            <Text style={Style.emptytext}>Sorry, no results found!</Text>
                            <Text style={Style.emptytext1}>Please check the spelling or try searching for something else</Text>
                        </View>
                    }
                    {this.state.loading ? <Base /> : null}
                    <SnackBar
                        visible={!this.state.isConnected}
                        backgroundColor={"red"}
                        textMessage="No Internet Connection" />
                </View>
            </KeyboardAvoidingView>
        );
    }
    Categorybg = (visible) => {
        this.setState({ modalcat: visible });
        this.setState({ Category: visible });
        this.setState({ brand: false });
        this.setState({ supplier: false });
        this.setState({ sort: false });
    }
    Brandbg = (visible) => {
        this.setState({ modalbrand: visible });
        this.setState({ brand: visible });
        this.setState({ supplier: false });
        this.setState({ sort: false });
        this.setState({ Category: false });
    }
    Supplierbg = (visible) => {
        this.setState({ modelsupplier: visible });
        this.setState({ supplier: visible });
        this.setState({ brand: false });
        this.setState({ sort: false });
        this.setState({ Category: false });
    }
    Sortbg = (visible) => {
        this.setState({ modelsort: visible });
        this.setState({ sort: visible });
        this.setState({ supplier: false });
        this.setState({ brand: false });
        this.setState({ Category: false });
    }
}

function newFunction(responseData) {
    return responseData.items.length;
}

function buildSelectedRows(items, selectedItems) {
    const rows = items.map(item =>
        Object.assign({}, item, {
            selected: false,
            itemType: 1,
            qty: 1,
            totalItem: 0,
            radioIndex: null,
            wishselect: selectedItems.some(i => i.product_id == item.objectID),
        }),
    );
    return rows;
}