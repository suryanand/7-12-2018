import React, { Component } from 'react';
import {
    View,
    Text,
    ImageBackground,
    NetInfo,
    ActivityIndicator,
    Image,
    FlatList,
    ListView,
    TouchableOpacity
} from 'react-native'
import Style from './HomeStyle';
import Base from './Base';
import CardView from 'react-native-cardview';
import ServiceCall from "../../service/Service";
import Icon from "react-native-vector-icons/SimpleLineIcons";
import FCM, { NotificationActionType } from "react-native-fcm";
import { registerKilledListener, registerAppListener } from "../../service/Listeners";
import SnackBar from 'react-native-snackbar-component';
import SInfo from "react-native-sensitive-info";
import SideMenu from "../sideMenu/SideMenu";
import sortJsonArray from 'sort-json-array';
export default class Home extends Base {

    constructor() {
        super();
        this.onEndReachedCalledDuringMomentum = true;
        this.state = {
            page: 10,
            bestsellers: [],
            categories: [],
            isRefreshing: false,
            isConnected: true,
            index: 0,
            selectData: [],
            loading: false,
        };
    }

    handleConnectivityChange = isConnected => {
        if (isConnected) {
            this.setState({ isConnected });
        } else {
            this.setState({ isConnected });
        }
    };

    handleLoadMore = () => {
        if (this.state.bestsellers.length > 5) {
            if (!this.onEndReachedCalledDuringMomentum) {
                this.setState(state => ({
                    isRefreshing: true
                }), () => this.loadUsers());
                this.onEndReachedCalledDuringMomentum = true;
            }
        }
    };

    loadUsers() {
        const { page } = this.state;
        ServiceCall.getInstance().getHomeData(page, this.OnRequestCompletedListener, "Home");
    }

    OnRequestCompletedListener = (responseData, reqName) => {
        let cate = [];
        if (reqName == "Home") {
            data = sortJsonArray(responseData.categories, 'name', 'asc');
            this.setState({
                bestsellers: [...this.state.bestsellers, ...responseData.bestsellers],
                categories: data,
                isRefreshing: false,
            });
        }
    }


    componentDidMount() {
        NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
        this.loadUsers();
        super._permissions();
        SideMenu.getName();
        registerAppListener(this.props.navigation);
        registerKilledListener(this.props.navigation);
    }

    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
    }

    componentWillMount() {
        FCM.getInitialNotification().then(notif => {
            if (notif.id != undefined) {
                this.props.navigation.navigate("Notification");
            }
        });
    }

    renderRowItem = (rowData) => {
        return (
            <CardView
                cardElevation={5}
                cornerRadius={3}
                style={Style.offerslist}>
                <TouchableOpacity
                    onPress={() => this.moveToAll("BEST", 0)}>
                    <Text style={Style.text1}>{rowData.name}</Text>
                </TouchableOpacity>
            </CardView>
        );
    }

    renderFooter() {
        return this.state.isRefreshing ? <ActivityIndicator size="small" animating /> : null
    }
    moveToAll = (data, index) => {
        let req = [];
        let json = {};
        let ind;

        if ("BEST" == data) {
            json["value"] = 0;
            json["label"] = "{value:BestSeller,selected:true}";
            req.push(json);
            ind = 1
        } else if ("ALL" == data) {
            ind = 2;
        } else {
            json["value"] = index;
            json["label"] = data;
            req.push(json);
            ind = 3;
        }
        this.props.navigation.navigate("All", {
            data: req,
            index: ind
        });
    }

    render() {

        const { bestsellers, isRefreshing, categories } = this.state;
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

        return (
            <ImageBackground source={{ uri: 'http://melonberries.com/agentx/appimages/background.png' }} style={Style.root}>
                <View style={Style.root}>
                    <View style={Style.view1}>
                        <TouchableOpacity
                            style={Style.titleView} onPress={() => this.moveToAll("BEST", 0)}>
                            <Text style={Style.text}>Best Sellers</Text>
                            <Icon name="arrow-right" size={26} color="#FF3366" />
                        </TouchableOpacity>
                    </View>
                    <View style={Style.view2}>
                        <ListView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            dataSource={ds.cloneWithRows(bestsellers)}
                            onEndReached={() => this.handleLoadMore()}
                            onEndReachedThreshold={5}
                            onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false }}
                            renderFooter={() => this.renderFooter()}
                            renderRow={(rowData, sectionID, rowID, highlightRow) => this.renderRowItem(rowData)

                            }
                        />

                    </View>
                    <View style={Style.view1}>
                        <TouchableOpacity
                            onPress={() => this.moveToAll("ALL", "")}
                            style={Style.titleView}>
                            <Text style={Style.text}>All Products</Text>
                            <Icon name="arrow-right" size={26} color="#FF3366" />
                        </TouchableOpacity>
                    </View>
                    <View style={Style.view3}>
                        <FlatList
                            data={categories}
                            numColumns={2}
                            showsVerticalScrollIndicator={false}
                            renderItem={({ item, index }) =>
                                <CardView
                                    cardElevation={3}
                                    cornerRadius={3}
                                    style={Style.all_product}>
                                    <TouchableOpacity
                                        onPress={() => this.moveToAll(item, index)}
                                        style={Style.button}>
                                        <Image source={{ uri: 'http://www.melonberries.com/agentx/media/catalog/category/' + item.image }} style={Style.button_icon} />
                                        <Text style={Style.button_text}>{item.name}</Text>
                                    </TouchableOpacity>
                                </CardView>}
                            keyExtractor={i => i.name} />
                    </View>
                    {this.state.loading ? <Base /> : null}
                    <SnackBar
                        visible={!this.state.isConnected}
                        backgroundColor={"red"}
                        textMessage="No Internet Connection" />
                </View>
            </ImageBackground>);

    }
}
