import { ResFontSizes, ResHeight, ResWidth } from "../../ui";
export default {
    container: {
        flex: 1,
    },
    markWrap: {
        flex: 4,
        alignItems: "center",
        justifyContent: "center",

    },
    mark: {
        width: ResWidth(40),
        height: ResWidth(40),
        flex: 1,
    },
    background: {
        width: ResWidth(100),
        height: ResHeight(100),
    },
    wrapper: {
        flex: 6,
        margin: ResWidth(12)
    },
    inputWrap: {
        flexDirection: "row",
        marginVertical: 10,
        height: 40,
        borderBottomWidth: 1,
        borderBottomColor: "#CCC"
    },
    iconWrap: {
        paddingHorizontal: 7,
        alignItems: "center",
        justifyContent: "center",
    },
    icon: {
        height: ResWidth(8),
        width: ResWidth(8),
    },
    input: {
        flex: 1,
        paddingHorizontal: 10,
        fontFamily:"Montserrat-Regular"
    },
    button: {
        backgroundColor: "#FF3366",        
        borderRadius: 3,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 30,
    },
    buttonText: {
        color: "#FFF",
        padding:12,
        fontSize: ResFontSizes(2),
        fontFamily:"Montserrat-Regular"
    },

};