import React from "react";
import { TouchableOpacity, InteractionManager } from 'react-native';
import PropTypes from 'prop-types';
import Icon from "react-native-vector-icons/MaterialIcons"
import { StackActions, NavigationActions } from "react-navigation";
const MyOrderBack = ({ navigation }) => (
    <TouchableOpacity
        style={{ marginLeft: 10 }}
        onPress={() => goBack(navigation)}>
        <Icon name="arrow-back" size={26} color="#FFFFFF" />
    </TouchableOpacity>
);
function goBack(navigation) {
    const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Home' })],
    });
    InteractionManager.runAfterInteractions(() => {
        navigation.dispatch(resetAction);
    });
}
MyOrderBack.propTypes = {
    navigation: PropTypes.object.isRequired
};

export default MyOrderBack;
