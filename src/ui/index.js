import { Dimensions } from 'react-native'
import BackButton from './BackButton';
import DrawerButton from './DrawerButton';
import LinearFradient from "./LinearGradient";
import MyOrderBack from "./MyOrderBack";
export { BackButton, DrawerButton, LinearFradient, MyOrderBack }
export default { BackButton, DrawerButton, LinearFradient, MyOrderBack }


const { height, width } = Dimensions.get('window');

export const ResHeight = (h) => {
  return height * (h / 100);
};

export const ResWidth = (w) => {
  return width * (w / 100);
};

export const ResFontSizes = (f) => {
  return Math.sqrt((height * height) + (width * width)) * (f / 100);
};