import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import { Dimensions } from 'react-native'
import Login from '../pages/login/Login';
import { Home, AllProduct, WishList, MyCart, MainScreen, ReviewOrder, MyOrder, EditerOrder, Success, Notification, ViewOrder } from '../pages/home/index';
import Style from '../style/Styles';
import { createStackNavigator } from "react-navigation";
import Icon from "react-native-vector-icons/EvilIcons";
import { DrawerButton, BackButton, LinearFradient, MyOrderBack } from '../ui';
const { height, width } = Dimensions.get('window');
export const stackNavigator = (signedIn = false) => {

  renderAllProduct = (navigation, count, Key, flag) => {
    return (
      <View style={Style.headerView}>
        <Icon
          size={36}
          color={"white"}
          name="search"
          style={Style.rightlastIcon}
          onPress={() => {
            AllProduct.searchMethod(!flag)
          }} />
        <Icon
          size={36}
          color={"white"}
          name="heart"
          style={Style.rightlastIcon}
          onPress={() => {
            navigation.navigate("Wishlist");
          }}
        />

        <TouchableOpacity onPress={() => {
          navigation.navigate("MyCart", { callBack: this.callBack });
        }}>
          <Icon
            size={36}
            color={"white"}
            name="cart"
            style={Style.rightlastIcon}
          />
          {count > 0 ?
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: "green",
                position: "absolute",
                top: 0,
                marginRight: 0,
                width: 18,
                height: 18,
                borderRadius: 9,
              }}>
              <Text
                style={{
                  fontSize: 13,
                  color: "white",
                  fontWeight: "bold",
                  alignSelf: "center"
                }}>{count}</Text>
            </View> : null
          }
        </TouchableOpacity>


      </View>
    );
  }

  renderWishlist = (navigation, count, key) => {
    return (
      <View style={Style.headerView}>
        {/* <Icon
          size={36}
          color={"white"}
          name="search"
          style={Style.rightlastIcon}
          onPress={() => {
            navigation.navigate("Wishlist");
          }}
        /> */}
        {this.renderSearch(navigation, count, key)}
      </View>
    )
  }
  renderMyCart = (navigation, count, key) => {
    return (
      <View style={Style.headerView}>
        {this.renderSearch(navigation, count, key)}
      </View>
    );
  }
  renderSearch = (navigation, count, Key) => {
    return (
      <View>
        <TouchableOpacity onPress={() => {
          navigation.navigate("MyCart");
        }}>
          <Icon
            size={36}
            color={"white"}
            name="cart"
            style={Style.rightlastIcon}
          />
          {count > 0 ?
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: "green",
                position: "absolute",
                top: 0,
                marginRight: 0,
                width: 18,
                height: 18,
                borderRadius: 9,
              }}>
              <Text
                style={{
                  color: "white",
                  fontWeight: "bold",
                  alignSelf: "center"
                }}>{count}</Text>
            </View> : null
          }
        </TouchableOpacity>
      </View>
    );
  }
  return createStackNavigator(
    {
      login: {
        screen: Login,
        navigationOptions: {
          headerLeft: null,
          drawerLabel: () => null
        }
      },
      MainScreen:
      {
        screen: MainScreen,
        headerMode: "none",
        navigationOptions: {
          headerLeft: null,
          drawerLockMode: "locked-closed",

        }
      },
      Home: {
        screen: Home,
        navigationOptions: ({ navigation }) => ({
          headerLeft: (
            <View style={Style.homenavtitle}>
              <View style={Style.leftContainer}>
                <DrawerButton navigation={navigation} />
              </View>
              <Image
                source={{ uri: 'http://melonberries.com/agentx/appimages/logo.png' }}
                style={{ width: 50, height: 50, borderRadius: 50 / 2, backgroundColor: '#ffffff' }}
                resizeMode='cover' />
              <View style={Style.rightContainer}>
              </View>
            </View>
          ),
          headerBackground: (
            <LinearFradient />
          ),
          // headerRight: (
          //   <Icon
          //     size={36}
          //     color={"#000000"}
          //     name="search"
          //     style={Style.rightIcon}
          //     onPress={() => {
          //       navigation.navigate("Search");
          //     }}
          //   />
          // )
        })
      },
      All: {
        screen: AllProduct,
        navigationOptions: ({ navigation }) => ({
          headerLeft: (
            <View style={Style.navtitle}>
              <DrawerButton navigation={navigation} />
              <Text style={Style.navigationTitle}>All Products</Text>
            </View>),
          headerBackground: (
            <LinearFradient>
            </LinearFradient>
          ),
          headerRight: (
            this.renderAllProduct(navigation, navigation.getParam('count', []), "MyCart", navigation.getParam('flag', []))
          )
        })
      },
      Wishlist: {
        screen: WishList,
        navigationOptions: ({ navigation }) => ({
          headerLeft: (
            <View style={Style.navtitle}>
            <MyOrderBack navigation={navigation} />
              {/* <BackButton navigation={navigation} title={"wishlist"} /> */}
              <Text style={Style.navigationTitle}>My Wishlist</Text>
            </View>),
          headerBackground: (
            <LinearFradient />
          ),
          headerRight: (
            this.renderWishlist(navigation, navigation.getParam('Wishcount', []), "MyCart")
          )
        })
      },
      MyCart: {
        screen: MyCart,
        navigationOptions: ({ navigation }) => ({
          headerLeft: (
            <View style={Style.navtitle}>
              <MyOrderBack navigation={navigation} />
              <Text style={Style.navigationTitle}>My Cart</Text>
            </View>),
          headerBackground: (
            <LinearFradient />
          ),
          headerRight: (
            this.renderMyCart(navigation, navigation.getParam('count', []), "MyCart")
          )
        })
      },
      ReviewOrder: {
        screen: ReviewOrder,
        navigationOptions: ({ navigation }) => ({
          headerLeft: (
            <View style={Style.navtitle}>
              <BackButton navigation={navigation} title={"wishlist"}/>
              <Text style={Style.navigationTitle}>Review Order</Text>
            </View>),
          headerBackground: (
            <LinearFradient />
          ),
          headerRight: (
            this.renderMyCart(navigation, navigation.getParam('count', []), "MyCart")
          )
        })
      },
      MyOrder: {
        screen: MyOrder,
        navigationOptions: ({ navigation }) => ({
          headerLeft: (
            <View style={Style.navtitle}>
              <MyOrderBack navigation={navigation} />
              <Text style={Style.navigationTitle}>My Orders</Text>
            </View>),
          headerBackground: (
            <LinearFradient />
          ),
        })
      },
      viewOrder: {
        screen: ViewOrder,
        navigationOptions: ({ navigation }) => ({
          headerLeft: (
            <View style={Style.navtitle}>
              <BackButton navigation={navigation} />
              <Text style={Style.navigationTitle}>{"Order ID : " + navigation.getParam('edittitle', [])}</Text>
            </View>),
          headerBackground: (
            <LinearFradient />
          )
          // headerRight: (
          //   this.renderMyCart(navigation, navigation.getParam('count', []), "MyCart")
          // )
        })
      },
      success: {
        screen: Success,
        navigationOptions: {
          headerLeft: null,
          drawerLockMode: "locked-closed"
        }
        // navigationOptions: ({ navigation }) => ({
        //   headerLeft: (
        //     <View style={Style.navtitle}>
        //       <BackButton navigation={navigation} />
        //       <Text style={Style.navigationTitle}>My Orders</Text>
        //     </View>),
        //   headerBackground: (
        //     <LinearFradient />
        //   ),
        // })
      },
      EditerOrder: {
        screen: EditerOrder,
        navigationOptions: ({ navigation }) => ({
          headerLeft: (
            <View style={Style.navtitle}>
              <BackButton navigation={navigation} />
              <Text style={Style.navigationTitle}>{"Order ID : " + navigation.getParam('edittitle', [])}</Text>
            </View>),
          headerBackground: (
            <LinearFradient />
          ),
          // headerRight: (
          //  // this.renderMyCart(navigation, 3, "MyCart")
          // )
        })
      },
      Notification: {
        screen: Notification,
        navigationOptions: ({ navigation }) => ({
          headerLeft: (
            <View style={Style.navtitle}>
              <BackButton navigation={navigation} />
              <Text style={Style.navigationTitle}>Notifications</Text>
            </View>),
          headerBackground: (
            <LinearFradient />
          ),
          // headerRight: (
          //  // this.renderMyCart(navigation, 3, "MyCart")
          // )
        })
      },
    },
    {
      initialRouteName: signedIn ? "Home" : "MainScreen",
      headerMode: "screen",
      cardStyle: {
        backgroundColor: "transparent"
      }
    });
}
export default stackNavigator;